RISC [1] processor instructions knowledge base. Uses RDF [2] to store data.


### Typical usage


```
IMAGE=risc-knowledge-debian-bullseye
docker build -t $IMAGE docker/

docker run -v $(pwd):/shared -it $IMAGE
...
/shared# ./generate_risc_ttl.py
...
/shared# ./regenerate_ttl.pl uuids.ttl
```


### References

[1] https://en.wikipedia.org/wiki/Reduced_instruction_set_computer

[2] https://en.wikipedia.org/wiki/Semantic_triple
