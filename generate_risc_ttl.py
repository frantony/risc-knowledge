#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:ts=4:sw=4:softtabstop=4:smarttab:expandtab

from rdflib import Graph, Literal, Namespace, XSD
import uuid
import re
from tabulate import tabulate

from mips_r2_instructions import mips_r2_instructions


class kb(object):
    def __init__(self, ttlfile=None):
        self.g = Graph()
        self.ttlfile = ttlfile
        self.load(ttlfile)

        self.uuidns = Namespace("uuid:///")
        self.scmns = Namespace("terminusdb:///schema#")
        self.g.bind('uuid', self.uuidns)
        self.g.bind('scm', self.scmns)

    def load(self, ttlfile=None):
        if ttlfile is None:
            ttlfile = self.ttlfile
        if ttlfile is not None:
            self.g = Graph()
            self.g.parse(ttlfile, format="ttl")

    def save(self, ttlfile=None):
        if ttlfile is None:
            ttlfile = self.ttlfile
        assert(ttlfile is not None)
        f = open("%s" % (ttlfile,), "w")
        f.write(self.g.serialize(format="turtle").decode("utf-8"))
        f.close()

    def new_named_triple(self, name):
        t = str(uuid.uuid4())
        subj = self.uuidns[t]

        pred = self.scmns["name"]

        obj = Literal(name)

        self.g.add((subj, pred, obj))

        return subj

    def subj_isa_named_obj(self, subj, named_obj):
        subj = "uuid:" + subj.rsplit('uuid:///')[-1]
        query = """
INSERT { %s scm:is-a ?fam }
WHERE {
    ?fam scm:name "%s" .
}""" % (subj, named_obj)

        b.g.update(query)

    def new_named_obj(self, name, named_obj):
        t = self.new_named_triple(name)
        self.subj_isa_named_obj(t, named_obj)
        return t


def add_knowledge(b):
    def add_pred(b, name, isa, predname, predval):
        query = """
INSERT { ?subj scm:%s "%s" }
WHERE {
    ?subj scm:name "%s" .
    ?fam scm:name "%s" .
    ?subj scm:is-a ?fam .
}""" % (predname, predval, name, isa)
        qres = b.g.update(query)

    def assert_mips_ins(name):
        t = ins_to_uuid(b, name, "MIPS instruction")
        assert(t is not None)

    def add_mips_ins_pred(b, ins, predname, predval):
        assert_mips_ins(ins)
        add_pred(b, ins, "MIPS instruction", predname, predval)

    def ins_to_uuid(b, insname, instype):
        query = """
SELECT ?subj
WHERE {
    ?subj scm:name "%s" .
    ?fam scm:name "%s" .
    ?subj scm:is-a ?fam .
}""" % (insname, instype)
        qres = b.g.query(query)
        assert(len(qres) == 1)
        # HACK
        for i in qres:
            return i.subj
        return None

    b.new_named_triple("MIPS instruction")

    b.new_named_triple("MIPS32 instruction")
    b.new_named_triple("MIPS64 instruction")
    b.new_named_triple("MIPS32 Release 2 instruction")
    b.new_named_triple("MIPS64 Release 2 instruction")
    b.new_named_triple("Release 2 instruction")

    for i in mips_r2_instructions:
        b.new_named_obj(i, "MIPS instruction")

    def add_fullname_url(name, fullname, url):
        add_mips_ins_pred(b, name, "fullname", fullname)
        add_mips_ins_pred(b, name, "url", url)

    def add_mips32_ins(name, fullname, url):
        add_fullname_url(name, fullname, url)
        add_mips_ins_pred(b, name, "is-a", "MIPS32 instruction")

    def add_mips32r2_ins(name, fullname, url):
        add_fullname_url(name, fullname, url)
        add_mips_ins_pred(b, name, "is-a", "MIPS32 Release 2 instruction")

    def add_mips64_ins(name, fullname, url):
        add_fullname_url(name, fullname, url)
        add_mips_ins_pred(b, name, "is-a", "MIPS64 instruction")

    def add_mips64r2_ins(name, fullname, url):
        add_fullname_url(name, fullname, url)
        add_mips_ins_pred(b, name, "is-a", "MIPS64 Release 2 instruction")

    def add_mips64_mips32r2_ins(name, fullname, url):
        add_mips64_ins(name, fullname, url)
        add_mips_ins_pred(b, name, "is-a", "MIPS32 Release 2 instruction")

    def add_float_fmt_ins(pref, fullname, url):
        add_mips32_ins(pref + ".S", fullname, url)
        add_mips32_ins(pref + ".D", fullname, url)
        add_mips64_mips32r2_ins(pref + ".PS", fullname, url)

    # ABS.fmt
    add_float_fmt_ins("ABS", "Floating Point Absolute Value",
                      "MIPS64_Architecture_Volume_II.pdf#G8.997393")

    # ADD
    add_mips32_ins("ADD", "Add Word",
                   "MIPS64_Architecture_Volume_II.pdf#G9.1001787")

    # ADD.fmt
    add_float_fmt_ins("ADD", "Floating Point Add",
                      "MIPS64_Architecture_Volume_II.pdf#G10.997392")

    # ADDI
    add_mips32_ins("ADDI", "Add Immediate Word",
                   "MIPS64_Architecture_Volume_II.pdf#G11.997393")

    # ADDIU
    add_mips32_ins("ADDIU", "Add Immediate Unsigned Word",
                   "MIPS64_Architecture_Volume_II.pdf#G12.997393")

    # ADDU
    add_mips32_ins("ADDU", "Add Unsigned Word",
                   "MIPS64_Architecture_Volume_II.pdf#G13.997413")

    # ALNV.PS
    add_mips64_mips32r2_ins("ALNV.PS", "Floating Point Align Variable",
                            "MIPS64_Architecture_Volume_II.pdf#G14.997393")

    # AND
    add_mips32_ins("AND", "And",
                   "MIPS64_Architecture_Volume_II.pdf#G15.1000988")

    # ANDI
    add_mips32_ins("ANDI", "And Immediate",
                   "MIPS64_Architecture_Volume_II.pdf#G16.997393")

    # B
    # Unconditional Branch
    # Assembly Idiom
    # BEQ r0, r0, offset
    # MIPS64_Architecture_Volume_II.pdf#G17.997393

    # BAL
    # Branch and Link
    # Assembly Idiom
    # BGEZAL r0, offset
    # MIPS64_Architecture_Volume_II.pdf#G18.997394

    # BC1F
    # MIPS64_Architecture_Volume_II.pdf#G19.997355
    # BC1FL
    # MIPS64_Architecture_Volume_II.pdf#G20.997355
    # BC1T
    # MIPS64_Architecture_Volume_II.pdf#G21.997355
    # BC1TL
    # MIPS64_Architecture_Volume_II.pdf#G22.1001202

    # BC2F
    # MIPS64_Architecture_Volume_II.pdf#G23.997355
    # BC2FL
    # MIPS64_Architecture_Volume_II.pdf#G24.997355
    # BC2T
    # MIPS64_Architecture_Volume_II.pdf#G25.997355
    # BC2TL
    # MIPS64_Architecture_Volume_II.pdf#G26.997355

    add_mips32_ins("BEQ", "Branch on Equal",
                   "MIPS64_Architecture_Volume_II.pdf#G27.997393")

    add_mips32_ins("BEQL", "Branch on Equal Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G28.997393")

    add_mips32_ins("BGEZ", "Branch on Greater Than or Equal to Zero",
                   "MIPS64_Architecture_Volume_II.pdf#G29.997394")

    add_mips32_ins("BGEZAL", "Branch on Greater Than or Equal to Zero and Link",
                   "MIPS64_Architecture_Volume_II.pdf#G30.997394")

    add_mips32_ins("BGEZALL", "Branch on Greater Than or Equal to Zero and Link Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G31.997394")

    add_mips32_ins("BGEZL", "Branch on Greater Than or Equal to Zero Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G32.997394")

    add_mips32_ins("BGTZ", "Branch on Greater Than Zero",
                   "MIPS64_Architecture_Volume_II.pdf#G33.997394")

    add_mips32_ins("BGTZL", "Branch on Greater Than Zero Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G34.997394")

    add_mips32_ins("BLEZ", "Branch on Less Than or Equal to Zero",
                   "MIPS64_Architecture_Volume_II.pdf#G35.997394")

    add_mips32_ins("BLEZL", "Branch on Less Than or Equal to Zero Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G36.997394")

    add_mips32_ins("BLTZ", "Branch on Less Than Zero",
                   "MIPS64_Architecture_Volume_II.pdf#G37.997394")

    add_mips32_ins("BLTZAL", "Branch on Less Than Zero and Link",
                   "MIPS64_Architecture_Volume_II.pdf#G38.997394")

    add_mips32_ins("BLTZALL", "Branch on Less Than Zero and Link Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G39.997394")

    add_mips32_ins("BLTZL", "Branch on Less Than Zero Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G40.997394")

    add_mips32_ins("BNE", "Branch on Not Equal",
                   "MIPS64_Architecture_Volume_II.pdf#G41.997393")

    add_mips32_ins("BNEL", "Branch on Not Equal Likely",
                   "MIPS64_Architecture_Volume_II.pdf#G42.997393")

    add_mips32_ins("BREAK", "Breakpoint",
                   "MIPS64_Architecture_Volume_II.pdf#G43.997381")

    add_float_fmt_ins("C.cond", "Floating Point Compare",
                   "MIPS64_Architecture_Volume_II.pdf#G44.997381")

    add_mips32_ins("CACHE", "Perform Cache Operation",
                   "MIPS64_Architecture_Volume_II.pdf#G45.997381")

#CEIL.L.fmt
#                   "MIPS64_Architecture_Volume_II.pdf#G46.997381")
#CEIL.W.fmt
#                   "MIPS64_Architecture_Volume_II.pdf#G47.997381")

    add_mips32_ins("CFC1", "Move Control Word From Floating Point",
                   "MIPS64_Architecture_Volume_II.pdf#G48.997381")

    add_mips32_ins("CFC2", "Move Control Word From Coprocessor 2",
                   "MIPS64_Architecture_Volume_II.pdf#G49.997381")

    add_mips32_ins("CLO", "Count Leading Ones in Word",
                   "MIPS64_Architecture_Volume_II.pdf#G50.997381")

    add_mips32_ins("CLZ", "Count Leading Zeros in Word",
                   "MIPS64_Architecture_Volume_II.pdf#G51.997381")

    add_mips32_ins("COP2", "Coprocessor Operation to Coprocessor 2",
                   "MIPS64_Architecture_Volume_II.pdf#G52.997381")

    add_mips32_ins("CTC1", "Move Control Word to Floating Point",
                   "MIPS64_Architecture_Volume_II.pdf#G53.997381")

    add_mips32_ins("CTC2", "Move Control Word to Coprocessor 2",
                   "MIPS64_Architecture_Volume_II.pdf#G54.997381")

# SWL
#CVT.D.fmt
# "Floating Point Convert to Double Floating Point"
#                   "MIPS64_Architecture_Volume_II.pdf#G55.997381")
# SD
#CVT.L.fmt
# "Floating Point Convert to Long Fixed Point"
#                   "MIPS64_Architecture_Volume_II.pdf#G56.997381")
#
#CVT.PS.S
# "Floating Point Convert Pair to Paired Single"
#                   "MIPS64_Architecture_Volume_II.pdf#G57.997381")
# DWL
#CVT.S.fmt
# "Floating Point Convert to Single Floating Point"
#                   "MIPS64_Architecture_Volume_II.pdf#G58.997381")
#CVT.S.PL
# "Floating Point Convert Pair Lower to Single Floating Point"
#                   "MIPS64_Architecture_Volume_II.pdf#G59.997381")
#CVT.S.PU
# "Floating Point Convert Pair Upper to Single Floating Point"
#                   "MIPS64_Architecture_Volume_II.pdf#G60.997381")
#
# SD
#CVT.W.fmt
# "Floating Point Convert to Word Fixed Point"
#                   "MIPS64_Architecture_Volume_II.pdf#G61.997381")

    # DADD
    add_mips64_ins("DADD", "Doubleword Add",
                   "MIPS64_Architecture_Volume_II.pdf#G62.997355")

    # DADDI
    add_mips64_ins("DADDI", "Doubleword Add Immediate",
                   "MIPS64_Architecture_Volume_II.pdf#G63.997355")

    # DADDIU
    add_mips64_ins("DADDIU", "Doubleword Add Immediate Unsigned",
                   "MIPS64_Architecture_Volume_II.pdf#G64.997355")

    # DADDU
    add_mips64_ins("DADDU", "Doubleword Add Unsigned",
                   "MIPS64_Architecture_Volume_II.pdf#G65.997355")

    # DCLO
    add_mips64_ins("DCLO", "Count Leading Ones in Doubleword",
                   "MIPS64_Architecture_Volume_II.pdf#G66.997356")

    # DCLZ
    add_mips64_ins("DCLZ", "Count Leading Zeros in Doubleword",
                   "MIPS64_Architecture_Volume_II.pdf#G67.997356")

    # DDIV
    add_mips64_ins("DDIV", "Doubleword Divide",
                   "MIPS64_Architecture_Volume_II.pdf#G68.997355")

    # DDIVU
    add_mips64_ins("DDIVU", "Doubleword Divide Unsigned",
                   "MIPS64_Architecture_Volume_II.pdf#G69.997355")

    # DERET
    # Debug Exception Return
    # EJTAG instruction
    # This instruction is legal only if the processor is executing in Debug Mode.
    # MIPS64_Architecture_Volume_II.pdf#G70.997388

    # DEXT
    add_mips64r2_ins("DEXT", "Doubleword Extract Bit Field",
                   "MIPS64_Architecture_Volume_II.pdf#G71.997845")

    # DEXTM
    add_mips64r2_ins("DEXTM", "Doubleword Extract Bit Field Middle",
                   "MIPS64_Architecture_Volume_II.pdf#G72.997845")

    # DEXTU
    add_mips64r2_ins("DEXTU", "Doubleword Extract Bit Field Upper",
                   "MIPS64_Architecture_Volume_II.pdf#G73.997845")

    # DI
    add_mips32r2_ins("DI", "Disable Interrupts",
                   "MIPS64_Architecture_Volume_II.pdf#G74.997845")

    # DINS
    add_mips64r2_ins("DINS", "Doubleword Insert Bit Field",
                   "MIPS64_Architecture_Volume_II.pdf#G75.997845")

    # DINSM
    add_mips64r2_ins("DINSM", "Doubleword Insert Bit Field Middle",
                   "MIPS64_Architecture_Volume_II.pdf#G76.997845")

    # DINSU
    add_mips64r2_ins("DINSU", "Doubleword Insert Bit Field Upper",
                   "MIPS64_Architecture_Volume_II.pdf#G77.997845")

    # DIV
    # DIV.fmt
    # DIVU
    # DMFC0
    # DMFC1
    # DMFC2
    # DMTC0
    # DMTC1
    # DMTC2
    # DMULT
    # DMULTU
    # DROTR
    # DROTR32
    # DROTRV
    # DSBH
    # DSHD
    # DSLL
    # DSLL32
    # DSLLV
    # DSRA
    # DSRA32
    # DSRAV
    # DSRL
    # DSRL32
    # DSRLV
    # DSUB
    # DSUBU
    # EHB
    # EI
    # ERET
    # EXT
    # FLOOR.L.fmt
    # FLOOR.W.fmt
    # INS
    # J
    # JAL
    # JALR
    # JALR.HB
    # JR
    # JR.HB
    # LB
    # LBU
    # LD
    # LDC1
    # LDC2
    # LDL
    # LDR
    # LDXC1
    # LH
    # LHU
    # LL
    # LLD
    # LUI
    # LUXC1
    # LW
    # LWC1
    # LWC2
    # LWL
    # LWR
    # LWU
    # LWXC1
    # MADD
    # MADD.fmt
    # MADDU
    # MFC0
    # MFC1
    # MFC2
    # MFHC1
    # MFHC2
    # MFHI
    # MFLO
    # MOV.fmt
    # MOVF
    # MOVF.fmt
    # MOVN
    # MOVN.fmt
    # MOVT
    # MOVT.fmt
    # MOVZ
    # MOVZ.fmt
    # MSUB
    # MSUB.fmt
    # MSUBU
    # MTC0
    # MTC1
    # MTC2
    # MTHC1
    # MTHC2
    # MTHI
    # MTLO
    # MUL
    # MUL.fmt
    # MULT
    # MULTU
    # NEG.fmt
    # NMADD.fmt
    # NMSUB.fmt
    # NOP
    # NOR
    # OR
    # ORI
    # PLL.PS
    # PLU.PS
    # PREF
    # PREFX
    # PUL.PS
    # PUU.PS
    # RDHWR
    # RDPGPR
    # RECIP.fmt
    # ROTR
    # ROTRV
    # ROUND.L.fmt
    # ROUND.W.fmt
    # RSQRT.fmt
    # SB
    # SC
    # SCD
    # SD
    # SDBBP
    # SDC1
    # SDC2
    # SDL
    # SDR
    # SDXC1
    # SEB
    # SEH
    # SH
    # SLL
    # SLLV
    # SLT
    # SLTI
    # SLTIU
    # SLTU
    # SQRT.fmt
    # SRA
    # SRAV
    # SRL
    # SRLV

    # SSNOP
    add_mips32_ins("SSNOP", "Superscalar Inhibit NOP",
                   "MIPS64_Architecture_Volume_II.pdf#G217.997413")

    # SUB
    # SUB.fmt
    # SUBU
    # SUXC1
    # SW
    # SWC1
    # SWC2
    # SWL
    # SWR
    # SWXC1
    # SYNC
    # SYNCI
    # SYSCALL
    # TEQ
    # TEQI
    # TGE
    # TGEI
    # TGEIU
    # TGEU
    # TLBP
    # TLBR
    # TLBWI
    # TLBWR
    # TLT
    # TLTI
    # TLTIU
    # TLTU
    # TNE
    # TNEI
    # TRUNC.L.fmt
    # TRUNC.W.fmt

    # WAIT
    #Enter Standby Mode

    # WRPGPR
    #Write to GPR in Previous Shadow Set

    # WSBH
    #Word Swap Bytes Within Halfwords

    # XOR
    #Exclusive OR

    # XORI
    #Exclusive OR Immediate


b = kb()

add_knowledge(b)

query = """
SELECT ?name ?fullname
WHERE {
    {
        ?a scm:name ?name .
        ?fam scm:name "MIPS instruction" .
        ?a scm:is-a ?fam .
    }

    OPTIONAL {
        ?a scm:name ?name .
        ?a scm:fullname ?fullname .
        ?fam scm:name "MIPS instruction" .
        ?a scm:is-a ?fam .
    }
}"""

qres = b.g.query(query)
ins = []
for row in qres:
    if row.fullname is None:
        fullname = ""
    else:
        fullname = row.fullname
    ins.append((f"{row.name}", fullname))

table = []
for i in sorted(ins):
    table.append(i)

headers = ["mnemonic", "instruction"]
print("total %d MIPS instructions" % (len(table),))
print(tabulate(table, headers=headers, tablefmt="grid"))

b.save("uuids.ttl")
