#!/usr/bin/env swipl
% vim:ts=4:sw=4:softtabstop=4:smarttab:expandtab:filetype=prolog

:- initialization(run, main).

:- use_module(library(semweb/turtle)).
:- use_module(library(semweb/rdf_db)).

:- use_module(library(optparse)).


:- rdf_register_prefix(scm, 'terminusdb:///schema#').

usage :-
    format("Usage:\n"),
    format("  regenerate_ttl.pl <ttl-file>\n").

run :-
    opt_arguments([], [], LIST),
    ( nth0(0, LIST, UUIDS) ; usage, halt ),
    rdf_reset_db,
    rdf_load(UUIDS),
    string_concat(UUIDS, ".regenerated", NewUUIDs),
    rdf_save_turtle(NewUUIDs, [comment(false), group(false)]).
